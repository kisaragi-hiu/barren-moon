let cjkFallback = ["Noto Sans CJK", "Noto Sans TC", "Microsoft Jhenghei"];

module.exports = {
  content: ["./templates/**/*.html"],
  theme: {
    fontFamily: {
      sans: ["Overpass", ...cjkFallback, "sans-serif"],
      cjk: [...cjkFallback, "sans-serif"],
      serif: [
        "'Noto Serif CJK'",
        "'Noto Serif TC'",
        "'Shippori Mincho'",
        "'Source Serif Pro'",
        // Never fallback to `serif` on a CJK page.
        // Otherwise the user will be served the PMingLiU eyesore
        // stack of fonts.
        "'Microsoft Jhenghei'",
        "sans-serif",
      ],
    },
    extend: {
      colors: {
        "grey-a": "#404040",
      },
      // fill: (theme) => ({
      //   accent: theme("colors.accent"),
      //   "accent-strong": theme("colors.accent-strong"),
      //   special: theme("colors.special"),
      //   "special-strong": theme("colors.special-strong"),
      // }),
      zIndex: { "-5": "-5" },
      // https://github.com/tailwindlabs/tailwindcss/discussions/1361
      boxShadow: {
        DEFAULT: "0 0 0.25rem #00000040",
        md: "0 0 0.25rem #00000070",
        white: "0 0 0.5rem #ffffff",
      },
    },
  },
  plugins: [],
};
