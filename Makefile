export LANG=en_US.UTF-8

.PHONY: dev-hugo dev-tailwind dev clean public
.DEFAULT_GOAL := public

static/css/built.css: src.css
	npx tailwindcss --postcss --minify -i src.css -o static/css/built.css

dev-hugo:
	hugo server

dev-tailwind:
	npx tailwindcss --postcss -i src.css -o static/css/built.css --watch

dev:
	npx concurrently "make dev-hugo" "make dev-tailwind"

clean:
	git clean -Xdf

public: static/css/built.css
	hugo --minify

public.tar: public
	(cd public && tar -cf ../public.tar .)
