# 如同月球般的荒涼

*Kisaragi Hiu poem collection 2013 ~ 2018*

Packaging up poems I wrote.

## License

See `LICENSE`. TL;DR: prose = CC-BY-SA 4.0, code = MIT.
