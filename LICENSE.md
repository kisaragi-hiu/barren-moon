# License

- All *prose writing* — comprised of all `.html.pm` files — are licensed under a Creative Commons Attribution-ShareAlike 4.0 International License, see file `LICENSE.cc-by-sa`.

- The *source code* — comprised of all `.ptree`, `.rkt`, `.yml`, `.html.p`, and `.css.pp` files — is licensed under the MIT license, see file `LICENSE.mit`.
