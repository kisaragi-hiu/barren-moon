Collapsed structures, left to fall apart on their own,
now all that’s left is a barren plain of dust.

Come take a look at a high schooler's heart. These are poems I wrote during high school.
