---
title: Sleepiness
date: 2017-12-25T10:19:24+09:00
---

I cannot do anything
other than showering anger
at myself

Diving into dreams, day and night
Looking for the nonexistent
redemption

Embracing the insecurity
contain it in my jacket
Feeling the imaginary warmth

Sleepiness comes
after exuding tears
